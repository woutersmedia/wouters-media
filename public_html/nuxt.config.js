export default {
  mode: 'universal',
  /*
  ** Headers of the page
  */
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      {charset: 'utf-8'},
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1'
      },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || ''
      },
      {
        name: 'application-name',
        content: 'Wouters Media'
      },
      {
        name: 'msapplication-config',
        content: '/icons/windows/browserconfig.xml'
      }
    ],
    link: [
      {
        rel: 'icon',
        type: 'image/x-icon',
        href: '/favicon.ico'
      },
      {
        rel: 'icon',
        type: 'image/png',
        href: '/icons/favicon/favicon-16x16.png',
        sizes: '16x16'
      },
      {
        rel: 'icon',
        type: 'image/png',
        href: '/icons/favicon/favicon-32x32.png',
        sizes: '32x32'
      },
      {
        rel: 'apple-touch-icon',
        type: 'image/png',
        href: '/icons/apple/apple-touch-icon.png',
        sizes: '180x180'
      },
      {
        rel: 'mask-icon',
        href: '/icons/apple/safari-pinned-tab.svg',
        color: '#17202a'
      },
      {
        rel: 'manifest',
        href: '/icons/android/site.webmanifest'
      }
    ],
    bodyAttrs: {
      class: 'aos-init'
    },
  },
  /*
  ** Customize the progress-bar color
  */
  loading: {color: '#ff9400'},
  /*
  ** Global CSS
  */
  css: [
    '@/assets/scss/style.scss',
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    {
      src: "@/plugins/aos",
      ssr: false
    }
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    '@nuxt/typescript-build',
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://buefy.github.io/#/documentation
    'nuxt-buefy',
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
  ],
  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  axios: {},
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
    }
  }
}
