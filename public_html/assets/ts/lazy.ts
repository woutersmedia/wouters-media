import VueLazyload from 'vue-lazyload';
import Vue from 'vue';

export class VueLazy {
  constructor() {
    Vue.use(VueLazyload)
  }
}
