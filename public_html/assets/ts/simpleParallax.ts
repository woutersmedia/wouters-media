// @ts-ignore
import * as SimpleParallax from 'simpleParallax';

// Parallax
const parallaxImages = document.getElementsByClassName('parallax');

console.log(parallaxImages);

new SimpleParallax(parallaxImages,{
    scale: 1.5,
});
