import AOS from "aos";

export class Aos {
  constructor() {
    AOS.init({
      once: true,
      startEvent: 'DOMContentLoaded',
      initClassName: 'aos-init'
    });
  }
}
